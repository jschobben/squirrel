const AotPlugin = require("@ngtools/webpack").AngularCompilerPlugin;
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const ContainerPlugin = require("webpack/lib/container/ContainerPlugin");

const squirrelConfig = {
    entry: ["./src/polyfills.ts", "./src/main.ts"],
    resolve: {
      mainFields: ["browser", "module", "main"]
    },
    devServer: {
      contentBase: path.join(__dirname, "dist/squirrel"),
      port: 3000
    },
    module: {
      rules: [
        { test: /\.ts$/, loader: "@ngtools/webpack" }
      ]
    },
    plugins: [
      new ContainerPlugin({
        name: "squirrel",
        filename: "remoteEntry.js",
        exposes: {
          Module: './src/app/squirrel/squirrel.module.ts'
        },
        library: { type: "var", name: "squirrel" },
        overridables: ["@angular/core", "@angular/common", "@angular/router"]
      }),
      new AotPlugin({
        skipCodeGeneration: false,
        tsConfigPath: "./tsconfig.app.json",
        directTemplateLoading: true,
        entryModule: path.resolve(
          __dirname,
          "./src/app/app.module#AppModule"
        )
      }),
      new HtmlWebpackPlugin({
        template: "./src/index.html"
      })
    ],
    output: {
      publicPath: "http://localhost:3000/",
      filename: "[name].js",
      path: __dirname + "/dist/squirrel",
      chunkFilename: "[id].[chunkhash].js"
    },
    mode: "production"
  };

  module.exports = squirrelConfig;